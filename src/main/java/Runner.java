import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {

        ChromeOptions options = new ChromeOptions();
        List<String> arguments = new ArrayList<>();
        arguments.add("headless");
        options.addArguments(arguments);
        String downloadDirectory = "C:\\Users\\u6065067\\Downloads";
        options.setExperimentalOption("download.default_directory", downloadDirectory);
//        options.setExperimentalOption("download.prompt_for_download", false);
//        options.setExperimentalOption("disable-popup-blocking", "true");
//        options.setExperimentalOption("block_third_party_cookies", true);

        ChromeDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.navigate().to("https://ocb-st.omv.com/ocb/app/");

//        HashSet<String> handles = (HashSet<String>) driver.getWindowHandles();
//        System.out.println("handles = " + handles.toString());

        String transparencyDataButtonXPath = "//*[@id=\"OCBA_B_DOOPENWORKSCREEN_COM-SIB-APP-OCB-SCR-CAPCHAWORSCR\"]/span/span";
        String exportButtonXPath = "//*[@id=\"CapCha-V5_SP1_P2_TP1_SP1_P1_B1\"]";

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(transparencyDataButtonXPath)));
        driver.findElement(By.xpath(transparencyDataButtonXPath)).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(exportButtonXPath)));
        driver.findElement(By.xpath(exportButtonXPath)).click();

//        HashSet<String> handles2 = (HashSet<String>) driver.getWindowHandles();
//        System.out.println("handles = " + handles2.toString());

        driver.close();

    }
}
